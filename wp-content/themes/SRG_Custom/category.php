<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

    <?php printf( __( 'Category Archives: %s', 'srg' ), '' . single_cat_title( '', false ) . '' ); ?>
    <?php
    /* Run the loop for the category page to output the posts. */
    get_template_part( 'loop', 'category' );
    ?>
				
<?php get_footer(); ?>