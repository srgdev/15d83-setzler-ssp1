<?php
/**
 * The Sidebar containing widget areas.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>

<?php
/* When we call the dynamic_sidebar() function, it'll spit out
 * the widgets for that widget area. If it instead returns false,
 * then the sidebar simply doesn't exist, so we'll hard-code in
 * some default sidebar stuff just in case.
 */
if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

		<?php get_search_form(); ?>

		<h3><?php _e( 'Archives', 'srg' ); ?></h3>
		<ul>
			<?php wp_get_archives( 'type=monthly' ); ?>
		</ul>

		<h3><?php _e( 'Meta', 'srg' ); ?></h3>
		<ul>
			<?php wp_register(); ?>
			<li><?php wp_loginout(); ?></li>
			<?php wp_meta(); ?>
		</ul>

<?php endif; // end primary widget area ?>