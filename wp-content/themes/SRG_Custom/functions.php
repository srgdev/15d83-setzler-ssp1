<?php
/*----------------------------------------------------------------------------------------------------
* StoneRidgeGroup Function File for WordPress
* Copyright 2014
* By: Calvin deClaisse-Walford
----------------------------------------------------------------------------------------------------*/

// Include JQuery and other scripts
function srg_enqueue_scripts(){
    // If not admin, add latest jQuery to frontend page headers
    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.3.2', true);
        wp_enqueue_script('jquery');
    }

    // Enque all our custom scripts and plugins
    wp_enqueue_script( 'srg_main', get_template_directory_uri() . '/js/main.js', array('jquery'), false, true );
    wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/js/html5shiv.js', array(), false, true );
    wp_enqueue_script( 'parsley', get_template_directory_uri() . '/js/parsley.js', array('jquery'), false, true );
    wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), false, true );

    // Pass a global javascript object that contains the correct URL to process AJAX requests
    wp_localize_script('srg_main', 'SRG_Ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' ), 'siteurl' => get_bloginfo('url')));
}

add_action('wp_enqueue_scripts', 'srg_enqueue_scripts');

/*----------------------------------------------------------------------------------------------------*/

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 */
 if ( ! function_exists( 'srg_setup' ) ):
function srg_setup() {

    // This theme uses post thumbnails
    add_theme_support( 'post-thumbnails' );

    // Set up some default thumbnail size
    $thumbnail_size = array();
    $thumbnail_size['width'] = 250;
    $thumbnail_size['height'] = 250;
    add_image_size( 'srg-default', $thumbnail_size['width'], $thumbnail_size['height'], true );

    // Add default posts and comments RSS feed links to head
    add_theme_support( 'automatic-feed-links' );

    // This theme uses wp_nav_menu() in one location
    register_nav_menus( array('header' => 'Main Menu') );
    register_nav_menus( array('footer' => 'Footer Menu') );

    // Make theme available for translation
    // Translations can be filed in the /languages/ directory
    load_theme_textdomain( 'srg', TEMPLATEPATH . '/languages' );

    $locale = get_locale();
    $locale_file = TEMPLATEPATH . "/languages/$locale.php";
    if ( is_readable( $locale_file ) )
        require_once( $locale_file );

}
endif;

add_action( 'after_setup_theme', 'srg_setup' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Makes some changes to the <title> tag, by filtering the output of wp_title().
 *
 * @param string $title Title generated by wp_title()
 * @param string $separator The separator passed to wp_title().
 * @return string The new title, ready for the <title> tag.
 */
function srg_filter_wp_title( $title, $separator ) {

    // Don't affect wp_title() calls in feeds.
    if ( is_feed() )
        return $title;

    // The $paged global variable contains the page number of a listing of posts.
    // The $page global variable contains the page number of a single post that is paged.
    // We'll display whichever one applies, if we're not looking at the first page.
    global $paged, $page;

    if ( is_search() ) {
        // If we're a search, let's start over:
        $title = sprintf( 'Search results for %s', '"' . get_search_query() . '"' );
        // Add a page number if we're on page 2 or more:
        if ( $paged >= 2 )
            $title .= " $separator " . sprintf( 'Page %s', $paged );
        // Add the site name to the end:
        $title .= " $separator " . get_bloginfo( 'name', 'display' );
        // We're done. Let's send the new title back to wp_title():
        return $title;
    }

    // Otherwise, let's start by adding the site name to the end:
    $title .= get_bloginfo( 'name', 'display' );

    // If we have a site description and we're on the home/front page, add the description:
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title .= " $separator " . $site_description;

    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 )
        $title .= " $separator " . sprintf( 'Page %s', max( $paged, $page ) );

    // Return the new title to wp_title():
    return $title;
}
add_filter( 'wp_title', 'srg_filter_wp_title', 10, 2 );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Sets the post excerpt length to 80 characters.
 *
 * @return int
 */
function srg_excerpt_length( $length ) {
   return 60;
}
add_filter( 'excerpt_length', 'srg_excerpt_length' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @return string "Continue Reading" link
 */
function srg_continue_reading_link() {
    return '';//' <a href="'.get_permalink().'">[…]</a>';
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and srg_continue_reading_link().
 *
 * @return string An ellipsis
 */
function srg_auto_excerpt_more( $more ) {
    return srg_continue_reading_link();
}
add_filter( 'excerpt_more', 'srg_auto_excerpt_more' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function srg_custom_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= srg_continue_reading_link();
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'srg_custom_excerpt_more' );

/*----------------------------------------------------------------------------------------------------*/

// Add post_type parameter to the wp_get_archives() function
function srg_custom_post_type_archive_where($where,$args){
    $post_type  = isset($args['post_type'])  ? $args['post_type']  : 'post';
    $where = "WHERE post_type = '$post_type' AND post_status = 'publish'";
    return $where;
}

add_filter( 'getarchives_where','srg_custom_post_type_archive_where',10,2);

/*----------------------------------------------------------------------------------------------------*/

/**
* Define a simple function to output our menu cleanly, adding donate links / logos if their settings have been set in backend
*
* @param string $location
* @uses wp_nav_menu()
*/
function srg_nav_menu($location = '') {

    $ulid = $location == 'header' ? 'nav' : 'footerNav';
    $items_wrap = '<ul id='.$ulid.'>';
    $items_wrap .= '%3$s';
    $items_wrap .= '</ul>';

    $args = array(
        'theme_location'  => $location,
        'menu'            => '',
        'container'       => false,
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => '',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => $items_wrap,
        'depth'           => 0,
        'walker'          => ''
    );

    wp_nav_menu( $args );

};
