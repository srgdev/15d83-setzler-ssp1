<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>	

    <?php the_title(); ?>
    
    <?php the_content(); ?>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>