<?php
/**
* Index/Blog page sidebar template
*/
?>

<div id="sidebar" class="newsSidebar">
    <?php if(!dynamic_sidebar('blog-sidebar')): ?> 
    <div class="sideItem searchItem">
        <form method="post" action="#">
            <input name="search" type="text" placeholder="SEARCH NEWS" class="field">
            <input name="submit" type="submit" value="&#xf002;" class="submit">
            <br class="clear">
        </form>
    </div>
    
   <div class="sideItem listItem">
        <h1>CATEGORIES</h1>
        <ul>
            <li><a href="#">category1</a></li>
            <li><a href="#">category2</a></li>
            <li><a href="#">category3</a></li>
            <li><a href="#">category4</a></li>
        </ul>
    </div>
    
    <div class="sideItem listItem">
        <h1>ARCHIVES</h1>
        <ul>
            <li><a href="#">month1</a></li>
            <li><a href="#">month2</a></li>
            <li><a href="#">month3</a></li>
            <li><a href="#">month4</a></li>
        </ul>
    </div>
    
    <div class="sideItem signup">
        <h1>GET INVOLVED</h1>
        <form method="post" action="#" data-validate="parsley">
            <input name="name" type="text" placeholder="Name" class="field" data-required="true">
            <input name="email" type="text" placeholder="Email" class="field" data-required="true" data-type="email">
            <input name="submit" type="submit" value="SUBMIT" class="submit bgcolor-tertiary">
        </form>
    </div>
    
    <div class="sideItem socWidget">
        <iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FFacebookDevelopers&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=628393553876847" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>
    </div>
    <?php endif; ?>
</div><!-- End Sidebar-->