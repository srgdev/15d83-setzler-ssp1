SMART SITE PRO README
@by Calvin deClaisse-Walford
@version 1.0
@license GNU/GPL

/-----------------------------------------------------------------------------/
INSTALLATION:
/-----------------------------------------------------------------------------/

This assumes you have already set up a WordPress installation on your site/server

1. In your WordPress Admin panel, navigate to Appearance->Themes and click 'Add New.'
2. Select 'Upload' from the header menu, choose the theme .zip file, and upload it.
3. Once uploaded, activate the theme.

/-----------------------------------------------------------------------------/
INITIAL SET UP
/-----------------------------------------------------------------------------/

After you have activated your theme, there are a few basic WordPress settings and actions you must take for theme to work correctly.

    // PAGES //

        1. Go to 'Pages' and create a 'Home' page - make sure you select the 'Home' or similar page template when creating the page.
        2. Create a 'News' page, leaving the template as 'default.'
        3. Create a 'Thankyou' page, add some copy to the content, select a page template like 'default' or 'nosidebar' and save

    // MENUS //

        You can add any menu items you want: pages, posts, external links, or custom post type (Event, e.g.) archive pages.

        1. Go to Appearance->Menus and create at least one menu.  Name it what you like, and add the Home and News pages to the menu.  Make sure you save it.
        2. In the same menus panel, At the bottom of the Menu Structure panel is a section called Menu Settings.  Check the boxes under "Theme Locations" for the location this menu should go.
        3. If Event post types are included in the theme, you can add a menu item to show an "Events" blog page by select 'Custom Post Type Archives' on the left side of the menu panel, check the box next to Events, and click 'Add to Menu.'

        You can create more than one menu if the theme has more than one menu location.  The tab in the menus panel title 'Manage Locations' can help you understand your menu structure.

    // WORDPRESS SETTINGS //

        There are a few WordPress settings that must be set in order for the theme to work properly:

        1. Settings->Reading: 'Front page displays': select 'A static page.'  Select your homepage you created earlier for the 'Front Page' and the news page you created for the 'Posts Page.'
        2. Settings->Reading: 'Blog Pages show at most': Usually 2 or 3, depending on the theme.  Often showing more than this can lead to readability or usability issues.
        3. Settings->Permalinks: Select 'Post Name' as the permalink structure under 'Common Settings,' then make sure you SAVE at the bottom of the page.

    // FIRSTCLASS SETTINGS //

        These themes come packaged with integration to SRG's FirstClass accounts.  They require some basic setup before you can generate widget forms for use.
        Without setup, form widgets will display an error letting you know you haven't set an account up.

        1. Navigate to Appearance->FirstClass Form Settings
        2. Follow the instructions to select or add a client.
        3. Select the 'Thankyou' page you created in the first step.
        3. Now your FirstClass widgets will display a form instead of an error.

After creating your two main pages, creating at least one menu and assigning it a location, setting reading and permalink settings, and setting up FirstClass, you can safely move on to customization.

/-----------------------------------------------------------------------------/
CUSTOMIZING THE THEME
/-----------------------------------------------------------------------------/

After you install and do the initial setup of your theme, you can begin customizing it with widgets, options, colors, page layouts, etc.
DO NOT CHANGE ANY SETTINGS FROM INITIAL SETUP, except FirstClass settings if client changes or a new one must be created.

    // WIDGETS //

        Each SmartSite Pro theme has custom widgets designed to work for it.

        1. Navigate to Appearance->Widgets
        2. You will see a selection of widgets on the left, and all the theme's sidebar locations on the right.
            Sidebars are where widgets are displayed, and aren't necessarily 'Side Bars' - they are locations to display widget content
        3. Drag and Drop widgets to the sidebars they should be displayed in, fill out necessary options, etc.
            Some widgets will have no options, as they are controlled by other options or functions (like FirstClass, search, etc)
            Certain widgets, such as the FirstClass Form Widget, will notify you that other options need to be set up before they can be used (if the options have not been set up yet.).

        AS OF WP 3.9, YOU CAN WORK WITH WIDGETS DIRECTLY IN THE THEME CUSTOMIZER AND PREVIEW THEIR APPEARANCE ON THE FLY. See Customizer section for more.

    // CUSTOMIZER //

        This is the most powerful feature of the theme - it allows you to change colors, theme options, social media accounts, and widgets in a live preview window.
        Most theme options, including email addresses, addresses, phone numbers, social media accounts, logos, and titles can be administrated here.

        1. Navigate to Appearance->Customize to get started.
            This panel will display sections of options on the left, and a preview on the right.

        2. Select a section to work in, and the panel will slide down to show available options.
            Color options will show a color wheel, an input for HEX color codes, and a 'Default' button to reset to default colors.
            Text options will be basic inputs or text areas.
            Images can be uploaded for use here, however, they do not access the global media library - so once you select an image, you cannot re-select it after loading ANOTHER image here.

        3. In WordPress 3.9, widgets can be modified, added, or removed in this window as well.  The last section will display widgets for all CURRENTLY VISIBLE SIDEBARS.
            You can click on links in your preview to navigate to different pages with different sidebars - WordPress will recognize the new sidebars loaded and automatically load the widgets and options for those visible sidebars.

        4. If you like the current options you have entered, always remember to click 'Save and Publish' at the top.  If not, none of the live preview changes will be saved or take effect.

    // PAGE TEMPLATES //

        Most themes come with basic page templates - Home, Default, and Nosidebar

        1. Homepage templates usually display a news feed and will have custom options inside the page editor.  Usually, content entered in the page editor will NOT display on homepage templates.
        2. Default and Nosidebar templates refer to pages that display content with or without a sidebar.  These WILL display content entered in the page editor.
        3. Other page templates are left up to your interpretation.  Some will show content, some will show content and templated content like forms.

    // POSTS, EVENTS, SLIDES //

        All of these are considered 'posts' by WordPress - events and slides are custom post types that share many similar qualities to regular posts:
            Content
            Title
            Featured Image

        1. Posts:
            Treat as regular WP posts, will use content, title, featured image, category, and tags

        2. Events:
            Typically support content, featured image, and title
            Have custom fields to determine event times - these are important and determine the display order of events
            May or may not support categories and tags depending on theme

            Events are displayed by using a 'Custom Post Type Archive,' which is an option under your Menus panel.  The last section of menu items is titled 'Custom Post Type Archives,' and will add a link to display
            these 'posts.'  an Archive is another term for 'News Feed' or 'Blog Page' that displays a list of whatever type of content.

            Events are automatically ordered by their 'Event Date' - NOT by their 'posted on' date, which is how WP normally orders posts.

        3. Slides:
            Support content, title, and featured image
            Usually on display if they have a featured image, as this is the whole point of a slide
            Content must be kept short and succinct otherwise breakage may occur #punchintheface
            May have a custom field where you can select a page the slide links to.

        Some Smartsite themes may use regular posts as slides, looking for recent posts with featured images.  If there is no 'Slides' section on the WP backend, this is likely the case.