<?php
/**
 * The template for displaying the footer.
 *
 */
?>

<div id="footerRow" class="row bgcolor-primary">
	<div class="rowInner">
    
    	<?php srg_nav_menu('footer'); ?>
    	
        <?php if(get_theme_mod('srg_theme_twitter') || get_theme_mod('srg_theme_facebook') || get_theme_mod('srg_theme_youtube') || get_theme_mod('srg_theme_instagram')): ?>
	        <div id="footerSocnet">
	        	<?php if(get_theme_mod('srg_theme_twitter')): ?><a target="_blank" href="http://www.twitter.com/<?php echo get_theme_mod('srg_theme_twitter'); ?>" class="txtcolor-tertiary"><i class="fa fa-twitter"></i></a><?php endif; ?>
	            <?php if(get_theme_mod('srg_theme_facebook')): ?><a target="_blank" href="<?php echo get_theme_mod('srg_theme_facebook'); ?>" class="txtcolor-tertiary"><i class="fa fa-facebook"></i></a><?php endif; ?>
	            <?php if(get_theme_mod('srg_theme_youtube')): ?><a target="_blank" href="http://www.youtube.com/user/<?php echo get_theme_mod('srg_theme_youtube'); ?>" class="txtcolor-tertiary"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
	            <?php if(get_theme_mod('srg_theme_instagram')): ?><a target="_blank" href="http://www.instagram.com/<?php echo get_theme_mod('srg_theme_instagram'); ?>" class="txtcolor-tertiary"><i class="fa fa-instagram"></i></a><?php endif; ?>
	        </div>
        <?php endif; ?>
        
        <?php if(get_theme_mod('srg_theme_disclaimer')): ?>
        <div id="paidfor"><span><?php echo get_theme_mod('srg_theme_disclaimer'); ?></span></div>
        <br>
        <?php endif; ?>
    
	</div> <!-- End rowInner -->
</div> <!-- End row -->
<?php wp_footer(); ?>
</body>
</html>