<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300|Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    
    <?php wp_head(); ?>
    
</head>

<body <?php body_class('bgcolor-primary'); ?>>

<div id="topRow" class="row bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_header_bg') ? get_theme_mod('srg_theme_header_bg') : get_template_directory_uri().'/images/bg-top.png'; ?>)">
	<div class="rowInner">
    	
        <div id="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_theme_mod('srg_theme_logo') ? get_theme_mod('srg_theme_logo') : get_template_directory_uri().'/images/logo.png'; ?>"></a></div>
        
        <?php if(get_theme_mod('srg_theme_twitter') || get_theme_mod('srg_theme_facebook') || get_theme_mod('srg_theme_youtube') || get_theme_mod('srg_theme_instagram')): ?>
        <div id="socnet">
        	<?php if(get_theme_mod('srg_theme_twitter')): ?><a target="_blank" href="http://www.twitter.com/<?php echo get_theme_mod('srg_theme_twitter'); ?>" class="txtcolor-tertiary"><i class="fa fa-twitter"></i></a><?php endif; ?>
	        <?php if(get_theme_mod('srg_theme_facebook')): ?><a target="_blank" href="<?php echo get_theme_mod('srg_theme_facebook'); ?>" class="txtcolor-tertiary"><i class="fa fa-facebook"></i></a><?php endif; ?>
	        <?php if(get_theme_mod('srg_theme_youtube')): ?><a target="_blank" href="http://www.youtube.com/user/<?php echo get_theme_mod('srg_theme_youtube'); ?>" class="txtcolor-tertiary"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
	        <?php if(get_theme_mod('srg_theme_instagram')): ?><a target="_blank" href="http://www.instagram.com/<?php echo get_theme_mod('srg_theme_instagram'); ?>" class="txtcolor-tertiary"><i class="fa fa-instagram"></i></a><?php endif; ?>
        </div>
        <?php endif; ?>
        
        <br class="clear">	
    
	</div> <!-- End rowInner -->
</div> <!-- End row -->

<div id="bannerRow" class="row bgcolor-banner">
	<div class="rowInner">
    	<?php if(is_front_page()): ?>
	    	<?php if(get_theme_mod('srg_theme_showslider') == 1):?>
	    		<?php get_template_part('slider'); ?>
	    	<?php endif; ?>
    	<?php endif; ?>
	</div> <!-- End rowInner -->
</div> <!-- End row -->

<div id="navRow" class="row">
	<div class="rowInner">
    
    	<?php srg_nav_menu('header'); ?>
    
	</div> <!-- End rowInner -->
</div> <!-- End row -->