<?php
/**
 * Template Name: Page no sidebar
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner">

        <div id="content">
            <?php if(have_posts()): while(have_posts()): the_post(); ?>
                <h1><?php echo strtoupper(get_the_title()); ?></h1>
                <?php if(has_post_thumbnail()): ?>
                    <div class="mainPic"><?php the_post_thumbnail(); ?></div>
                <?php endif; ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>