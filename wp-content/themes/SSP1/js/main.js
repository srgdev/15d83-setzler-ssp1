$(document).ready(function()
{
	// Hook into our signup form and send off to FirstClass
	$('#signupform .submit').click(function(e){
		e.preventDefault();
		if( $('#signupform').parsley('validate') == true){
			$('#formError').slideUp(function(){$(this).remove()});
			
			var url = $('#ajaxURL').val();
			var data = $('#signupform').serialize();
			
			$('.spinner').fadeIn();
			$('#signupform input').css('opacity', '0.5').attr('disabled', 'disabled');			
			setTimeout(function(){
				$.post(SRG_Ajax.ajaxurl, data, function(data){
					if(data == '0') {
						$('.spinner').fadeOut();
						$('#signupform input').css('opacity', '1');
						$('#signupform').after(' <span style="display:none;color:red;" id="formError">We were unable to process your information at this time.</span>');
						setTimeout(function(){$('#formError').fadeIn();}, 500);
						$('#signupform input').removeAttr('disabled');
					} else if (data == '1') {
						window.location = $('#redirect').val();
					}
					console.log(data);
					});
			}, 1000);
		};
	});
		
	// OFFSET BODY IF ADMIN BAR IS PRESENT
	function offsetBody(){
		var admin = $('body').hasClass('admin-bar');
		if(admin){
			var offset = $('#wpadminbar').height();
			$('body').css('margin-top', offset+'px');
		}
	}
	offsetBody();
	$(window).resize(function(){offsetBody();});
});

/*$('.clearinput').clearInput();
	
	$('.append').appendAround();
	
	// simple drop toggle //
	$('#dropBtn').click(function() {
  		$('#dropItem').slideToggle();
	});
	
	// toggle from multiple inputs with .active indicator //
	$('.toggleBtn').click(function() {
    	searchSlide();
	});
	function searchSlide() {
		if ($('#toggleItem').hasClass("active")) {
			$('#toggleItem').slideUp()
			$('#toggleItem').removeClass('active');
		} else {
			$('#toggleItem').slideDown();
			$('#toggleItem').addClass('active');
		}
	}
	
	//Equal Column Heights
	$(window).bind("load", function() {
        if ('#conBox.setColHeight') {
			var colHeight = Math.max($('#main.column').height(),$('#sidebar.column').height());
			$('.column').height(colHeight);
    	}
    });	
*/	