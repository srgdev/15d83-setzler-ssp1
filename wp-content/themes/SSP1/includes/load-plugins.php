<?php
/**
 * Load packaged plugins, define some custom settings and globals
 * @uses srg_custom_fields()
 * @hook after_setup_theme
 */
function srg_plugin_loader(){
    define( 'ACF_LITE', true );
    include(TEMPLATEPATH . '/includes/plugins/advanced-custom-fields/acf.php');
    include(TEMPLATEPATH . '/includes/plugins/acf-repeater/acf-repeater.php');
    include(TEMPLATEPATH . '/includes/plugins/add-custom-post-types-archive-to-nav-menus/cpt-in-navmenu.php');
    
    // Setup custom fields
    srg_custom_fields();
}
add_action( 'after_setup_theme', 'srg_plugin_loader' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Sets up custom fields for ACF plugin
 */
function srg_custom_fields(){
    if(function_exists("register_field_group"))
    {
        register_field_group(array (
            'id' => 'acf_events',
            'title' => 'Events',
            'fields' => array (
                array (
                    'key' => 'srg_cpt_events_cf',
                    'label' => 'Start Date',
                    'name' => 'start_date',
                    'type' => 'date_picker',
                    'instructions' => 'Select the start date for your event.',
                    'required' => 1,
                    'date_format' => 'yymmdd',
                    'display_format' => 'mm/dd/yy',
                    'first_day' => 1,
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'event',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'no_box',
                'hide_on_screen' => array (
                    0 => 'discussion',
                    1 => 'comments',
                ),
            ),
            'menu_order' => 0,
        ));
    }     
}
