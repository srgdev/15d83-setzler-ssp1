<?php
/**
* Register Sidebars
* 
* @uses register_sidebar()
*/
function srg_sidebars_init() {
	register_sidebar( array(
		'name' => __( 'Home / Single Page Sidebar', 'srg' ),
		'id' => 'home-box',
		'description' => __( 'The home and single page sidebar area', 'srg' ),
		'before_widget' => '<div class="sideItem">',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Blog Sidebar', 'srg' ),
		'id' => 'blog-sidebar',
		'description' => __( 'The blog page and posts sidebar', 'srg' ),
		'before_widget' => '<div class="sideItem">',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => '',
	) );
}

// Register sidebars in init
add_action( 'widgets_init', 'srg_sidebars_init' );