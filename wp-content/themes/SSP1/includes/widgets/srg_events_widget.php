<?php
/**
* SRG Events Widget - shows recent events post types in sidebar, controllable number and title
* by: Calvin deClaisse-Walford
* v: 1.0
*/

class SRG_Events_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Events_Widget', 'SRG Events Widget', array( 'description' => 'FirstClass Events Widget for sidebar')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		?>
		<?php echo $args['before_widget'];?>
        <div class="events">
             <h1><?php echo $instance['title']; ?></h1>
            <?php $eventsargs = array('post_type' => 'event', 'posts_per_page' => $instance['numposts'], 'meta_key' => 'start_date', 'orderby' => 'meta_value_num', 'order' => 'DESC'); ?>
            <?php $events = new WP_Query($eventsargs);?>
            <?php if($events->have_posts()): ?>
                 <?php while($events->have_posts()): $events->the_post(); ?>
                    <div class="event">
                        <div class="eventDate"><i class="fa fa-clock-o"></i> <?php echo date('F j, Y', strtotime(get_field('start_date'))); ?></div>
                        <h3><?php the_title(); ?></h3>
                        <p><?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>">[…]</a></p>
                    </div>
                 <?php endwhile; ?>
            <?php else: ?>
                <div class="noPosts">
                	<h1 class="noPostsMessage">Whoops!  No events exist yet!</h1>
                </div>   
            <?php endif; ?>
        </div>
        <?php echo $args['after_widget']; ?>
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Events';
		}
		
		if ( isset( $instance[ 'numposts' ] ) ) {
			$numposts = $instance[ 'numposts' ];
		} else {
			$numposts = '2';
		}
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
        <label for="<?php echo $this->get_field_id( 'numposts' ); ?>"><?php _e( 'Number of Posts' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'numposts' ); ?>" name="<?php echo $this->get_field_name( 'numposts' ); ?>" type="text" value="<?php echo esc_attr( $numposts ); ?>" />
		</p>
 
		<?php 


	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['numposts'] = ( ! empty( $new_instance['numposts'] ) ) ? strip_tags( $new_instance['numposts'] ) : '';
		return $instance;
		
	}
	
}

// Register and load the widget
function srg_events_widget_load() {
	register_widget( 'SRG_Events_Widget' );
}
add_action( 'widgets_init', 'srg_events_widget_load' );

?>