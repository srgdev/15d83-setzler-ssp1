<?php

/**
* SRG Archives Widget - Generate a list of archives by motnh for posts or custom post types
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Archive_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Archive_Widget', 'SRG Archive Widget', array( 'description' => 'Archive Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		?>
        <?php echo $args['before_widget']; ?>
            <div class="listItem">
            	<h1><?php echo $instance['title']; ?></h1>
                <ul>
                    <?php if(get_post_type() == 'post'): ?>
                	   <?php wp_get_archives(); ?>
            	   <?php else: ?>
            	       <?php $this->get_cpt_archives('event'); ?>
        	       <?php endif; ?>
                </ul>
            </div>
        <?php echo $args['after_widget']; ?> 
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Archives';
		}
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
		<?php 


	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
		
	}
	/**
	 * Get a montlhy archive list for a custom post type
	 * @param  string  $cpt  Slug of the custom post type
	 * @param  boolean $echo Whether to echo the output
	 * @return array         Return the output as an array to be parsed on the template level
	 */
	public function get_cpt_archives( $cpt, $echo = true )
	{
		global $wpdb; 
		$sql = $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_type = %s AND post_status = 'publish' GROUP BY YEAR(wp_posts.post_date), MONTH(wp_posts.post_date) ORDER BY wp_posts.post_date DESC", $cpt);
		$results = $wpdb->get_results($sql);
	
		if ( $results )
		{
			$archive = array();
			foreach ($results as $r)
			{
				$year = date('Y', strtotime( $r->post_date ) );
				$month = date('F', strtotime( $r->post_date ) );
				$month_num = date('m', strtotime( $r->post_date ) );
				$link = get_bloginfo('siteurl') . '/' . $cpt . '/' . $year . '/' . $month_num;
				$this_archive = array( 'month' => $month, 'year' => $year, 'link' => $link );
				array_push( $archive, $this_archive );
			}
	
			if( !$echo )
				return $archive;
			foreach( $archive as $a )
			{
				echo '<li><a href="' . $a['link'] . '">' . $a['month'] . ' ' . $a['year'] . '</a></li>';
			}
		}
		return false;
	}
	
}

// Register and load the widget
function srg_archive_widget_load() {
	register_widget( 'SRG_Archive_Widget' );
}
add_action( 'widgets_init', 'srg_archive_widget_load' );

?>