<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '15d83');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c6ZH_}2-|Xq?0^(BZd}V+57t<<+hZVX7-Np*d|Pu%`xQ=F,~-A_{EplP!3}u+]{G');
define('SECURE_AUTH_KEY',  '}K7Uqn^%$SY;R`Del06M]<ih0<4MA%~(eZ2iC!:f#H<NUDq,l2f||HB(@m/l+pbu');
define('LOGGED_IN_KEY',    'n#Q.}7J:K5/7f{1/]l-ZRV,<G8yJJ[-HD<VvNUeqtls(FmvZ.hL-x04Fj1~*TE8[');
define('NONCE_KEY',        'z|wO;oLYs[6$ta^LQqzd,+]JT&Q}Ws;5|poT65lIHj|1Y<#0ks|oLe91LyYi5vSt');
define('AUTH_SALT',        'F;s_E;^cgG|9K-%)QRu|&=1QC-|fI6YwwcvT%x>yh|/8~NOOJQoy714E3h9,Cjt)');
define('SECURE_AUTH_SALT', '/?-u,KL^1#<;BC#mLO|z(/Br@lQ#/P@}o87ymb`i!^ bjse9vC~9?VLtf87jP?Z9');
define('LOGGED_IN_SALT',   'zQB<fbR3P(sYIa_M[=w[8|SBcrZ{FG`T@bT}E<&]q=u53Oy6B,0JKkj,o9$<:,cm');
define('NONCE_SALT',       '8djFQ,`_P7Ygw8:accxUMY~!X;?EnS|7qN+T6t5rs?UOg%=KA#M]VwzTF9AmG+h2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
